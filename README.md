# OpenML dataset: humans_numeric

https://www.openml.org/d/1058

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

1. Title: Assessing the Reliability of a Human Estimator
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
This is a PROMISE Software Engineering Repository data set made publicly
available in order to encourage repeatable, verifiable, refutable, and/or
improvable predictive models of software engineering.

If you publish material based on PROMISE data sets then, please
follow the acknowledgment guidelines posted on the PROMISE repository
web page http://promisedata.org/repository .
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
(c) 2007 : Gary Boetticher  : boetticher AT uhcl DOT edu Phone: +1 (281) 283 8305
This data set is distributed under the
Creative Commons Attribution-Share Alike 3.0 License
http://creativecommons.org/licenses/by-sa/3.0/

You are free:

* to Share -- copy, distribute and transmit the work
* to Remix -- to adapt the work

Under the following conditions:

Attribution. You must attribute the work in the manner specified by
the author or licensor (but not in any way that suggests that they endorse
you or your use of the work).

Share Alike. If you alter, transform, or build upon this work, you
may distribute the resulting work only under the same, similar or a
compatible license.

* For any reuse or distribution, you must make clear to others the
license terms of this work.
* Any of the above conditions can be waived if you get permission from
the copyright holder.
* Apart from the remix rights granted under this license, nothing in
this license impairs or restricts the author's moral rights.


2. Sources
(a) Creator: Gary D. Boetticher
(b) Date: February 20, 2007
(c) Contact: boetticher AT uhcl DOT edu Phone: +1 (281) 283 8305

3. Donor: Gary D. Boetticher

4. Past Usage: This data was used for:

Boetticher, G., Lokhandwala, N., James C. Helm, Understanding the Human
Estimator, Second International Predictive Models in Software Engineering
(PROMISE) Workshop co-located at the 22nd IEEE International Conference on
Software Maintenance, Philadelphia, PA, September, 2006. More information is
available at http://nas.cl.uh.edu/boetticher/research.html

Since PROMISE 2006, the data set expanded by about 50 percent. The additional
tuples allowed us to divide the data into 3 major categories. Those who severely
underestimate (first 25 tuples). Those who accurately estimate (next 25 tuples).
And those who severely overestimate (last 25 tuples). The PROMISE 2007 experiments
compare the underestimators with the accurate estimators and the overestimators with
the accurate estimators.

5. Number of Instances: 75

6. Number of Attributes: 14 independent variables and 1 dependent variable

7. Attribute Information:

Numeric Degree:  This attribute refers to the level of education of the participant.
2=High School, 3=Bachelors, 4=Masters,5=Ph.D.

TechUGCourses: This refers to the number of technical undergraduate courses that
the participant has taken.

TechGCourses: This refers to the number of technical graduate courses that
the participant has taken.

MgmtUGCourses: This refers to the number of management undergraduate courses that
the participant has taken.

MgmtGCourses: This refers to the number of management graduate courses that
the participant has taken.

Total Workshops: This refers to the total number of workshops that
the participant has attended.

Total Conferences: This refers to the total number of conferences that
the participant has attended.

TotalLangExp: This refers to the total number of languages and experience in those
languages that the participant has.

Hardware Proj Mgmt Exp: This corresponds to the total amount of time that the
respondant has been estimating hardware projects.

Software Proj Mgmt Exp: This corresponds to the total amount of time that the
respondant has been estimating software projects.

No Of Hardware Proj Estimated: This refers to the total number of hardware projects
that the participant has estimated.

No Of Software Proj Estimated: This refers to the total number of software projects
that the participant has estimated.

Domain Exp: The domain experience refers to how much experience the participant has
in the oil and gas industry.

Procurement Industry Exp: The procurement industry experience refers to the amount
of time, in years, that the participant has regarding
procurement.

ABS((TotalEstimates-TotalActual)/TotalActual): This is the class variable. It
represents the overall relative error for the participant's
estimates.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1058) of an [OpenML dataset](https://www.openml.org/d/1058). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1058/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1058/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1058/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

